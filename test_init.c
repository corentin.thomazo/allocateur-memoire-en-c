#include "mem.h"
#include "common.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define NB_TESTS 10
void afficher_zone(void *adresse, size_t taille, int free) {
        printf("Zone %s, Adresse : %lx, Taille : %lu\n", free?"libre":"occupee",
            (unsigned long) adresse, (unsigned long) taille);
}

int main(int argc, char *argv[]) {
/*	fprintf(stderr, "Test réalisant de multiples fois une initialisation suivie d'une alloc max.\n"
			"Définir DEBUG à la compilation pour avoir une sortie un peu plus verbeuse."
 		"\n");
	for (int i=0; i<NB_TESTS; i++) {
		debug("Initializing memory\n");
		mem_init(get_memory_adr(), get_memory_size());
		alloc_max(get_memory_size());
	}

*/	// TEST OK
//	mem_init(get_memory_adr(), get_memory_size());
//	mem_alloc(128);
//	mem_show(&afficher_zone);
//	mem_free(memallou);
//	printf("\n\n");
//	mem_show(&afficher_zone);
//	mem_alloc(100);


	mem_init(get_memory_adr(), get_memory_size());
	int non_quit=1;
	int i;
	int j;
	void* mem;
	while (non_quit==1){
		printf("taper 0 si vous vouler quitter\n ");
		printf("taper 1 si vous vouler allouer de la mémoire\n ");
		printf("taper 2 si vous vouler libérer de la mémoire\n ");
		printf("taper 3 si vous vouler voir l'état de la mémoire\n ");
		scanf("%d",&i);
		switch (i){
			case 0:
				non_quit=0;
				break;
			case 1:
				printf("taper la memoire souhaiter\n ");
				scanf("%d",&j);
				void* memallou4=mem_alloc(j);
				printf("%p",memallou4);
				break;
			case 2:
				printf("taper l'adresse mémoire à suprimer (en hexa)\n ");
				scanf("%p",&mem);
				mem_free(mem);
				break;
			case 3:
				mem_show(&afficher_zone);
				break;
			default:
				non_quit=1;
				break;
		}

	}
	
	
	return 0;
}
// j'ai fais de nombreux test et debugger quelques endroits à voir si il en reste
// une étape importantes serait de rendre le code plus propre
// notament sur les pointeurs ou j'ai pas fait quelque chose de beau

