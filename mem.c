/* On inclut l'interface publique */
#include "mem.h"
#include"stdio.h"
#include <assert.h>
#include <stddef.h>
#include <string.h>

/* Définition de l'alignement recherché
 * Avec gcc, on peut utiliser __BIGGEST_ALIGNMENT__
 * sinon, on utilise 16 qui conviendra aux plateformes qu'on cible
 */
#ifdef __BIGGEST_ALIGNMENT__
#define ALIGNMENT __BIGGEST_ALIGNMENT__
#else
#define ALIGNMENT 16
#endif

/* structure placée au début de la zone de l'allocateur

   Elle contient toutes les variables globales nécessaires au
   fonctionnement de l'allocateur

   Elle peut bien évidemment être complétée
*/
struct allocator_header {
        size_t memory_size;
	mem_fit_function_t *fit;
	struct fb* first;
	
};

/* La seule variable globale autorisée
 * On trouve à cette adresse le début de la zone à gérer
 * (et une structure 'struct allocator_header)
 */
static void* memory_addr;

static inline void *get_system_memory_addr() {
	return memory_addr;
}

static inline struct allocator_header *get_header() {
	struct allocator_header *h;
	h = get_system_memory_addr();
	return h;
}

static inline size_t get_system_memory_size() {
	return get_header()->memory_size;
}

struct alloue {
	size_t size;
};

struct fb {
	size_t size;
	struct fb* next;

};


void mem_init(void* mem, size_t taille)
{
	
        memory_addr = mem;
        *(size_t*)memory_addr = taille;
	/* On vérifie qu'on a bien enregistré les infos et qu'on
	 * sera capable de les récupérer par la suite
	 */
	assert(mem == get_system_memory_addr());
	assert(taille == get_system_memory_size());
		struct allocator_header *h= get_header();
	mem_fit(&mem_fit_first);
	h->first=mem+sizeof(struct allocator_header);
	h->first->size=taille-sizeof(struct allocator_header);
	h->first->next=NULL;
	
}

void mem_show(void (*print)(void *, size_t, int)) {
	int first=sizeof(struct allocator_header);//récupère taille de l'allocator header
	void* pointeurmem= memory_addr+first;//
	struct allocator_header *h=memory_addr;
	struct fb *fbmem=h->first;
	size_t taille=h->memory_size;//récupère la taille alloué à mem init
	void* total= memory_addr+taille;//addresse qui est au registre mem init + taille
	
	while (pointeurmem<total) {
		size_t* suivant= pointeurmem;	//lie la taille enregistré
		if (pointeurmem==fbmem){
		print(pointeurmem,*suivant,1);
		fbmem=fbmem->next;} // affiche ce qui est demandé
		else{print(pointeurmem,*suivant,0);}
		pointeurmem=pointeurmem+ (*suivant); // bouge le pointeur à l'addresse suivante
		
	}
}

void mem_fit(mem_fit_function_t *f) {
	get_header()->fit = f;
}

void *mem_alloc(size_t taille) {
	struct allocator_header *h=memory_addr;
	size_t ZU = taille;
	size_t zuPad=taille;
	if(ZU % ALIGNMENT != 0){		//vérifier si on a besoin d'un padding
		zuPad = ZU + ALIGNMENT-(ZU % ALIGNMENT);		//ajouter le padding
	}
	//__attribute__((unused)) /* juste pour que gcc compile ce squelette avec -Werror */
	struct fb *firstFree =h->first;
	size_t taille_zo = zuPad+sizeof(size_t);
	/* A ce niveau-ci, nous sommes au début du PrimalFree */

	struct fb *fb=h->fit(firstFree, taille_zo);	// On cherche une ZL

	if(fb == NULL){		// Si il n'y a pas de ZL, on retourne null
		return NULL;
	}

	size_t taille_zl = fb->size;
	void* suivant_zl = fb->next;
	void* adresseOldFb = fb;
	struct alloue *alloue= adresseOldFb;
	alloue->size = taille_zo;			//on ecrit en memoire la taille de ZO

	if(taille_zl <= taille_zo + sizeof(struct fb)){// vérifie si la taille de ZL est pas assez grande pour faire une nouvelle ZL et une ZO
		if (h->first==adresseOldFb){	// vérifie si c'est la première de la liste chainé
			h->first=h->first->next;	// réaffecte la liste chainé
		}else{
			fb = h->first;				// sinon on cherche la la zone libre précédente à celle que l'on a affecté
		
			while(fb->next != adresseOldFb && fb->next != NULL){
				fb = fb->next;
			}
			fb->next =(void*) suivant_zl;
		}
	}else{ 								// si on la place de faire une ZL et une ZO
		struct fb *newfb= adresseOldFb+taille_zo;	// On crée la nouvelle ZL
		newfb->size =taille_zl-taille_zo;
		if (h->first==adresseOldFb){				// On vérifie si l'ancienne ZL est la première de la liste
			h->first=newfb;
		}else{										// sinon on va chercher le précédent et ajouter la nouvelle ZL dans la liste
		fb = h->first;
		
		while(fb->next != adresseOldFb && fb->next != NULL){
			fb = fb->next;
		}
		if(fb->next == NULL)
			return NULL;
		fb->next += taille_zo;
		}
	}
	return adresseOldFb + sizeof(size_t);
}

void mem_free(void* mem) {		// libération de la mémoire
	mem-=sizeof(struct alloue);
	struct allocator_header *h=memory_addr;
	struct fb *fb=h->first;
	void* mem_first=h->first;
	void* mem_fb=fb->next;
	void* addr_mem_alloue=mem;
	size_t* taille= addr_mem_alloue;
	if(mem<mem_first){	// vérifie que l'adresse alloue est inférieur à la première ZL
		struct fb *fbnew=mem;	// créer un nouvelle ZL
		fbnew->next=h->first;	
		h->first=mem;			// implémente la nouvelle ZL en first
		fbnew->size=(size_t)*taille;
		mem=mem+fbnew->size;
		if (mem==fbnew->next){	// vérifie si la zone suivant la ZO est libre si oui elle la fusionne
			fbnew->size=fbnew->size+fbnew->next->size;
			fbnew->next=fbnew->next->next;
		}
	}else{		
	while (mem_fb<mem){	// cherche la ZL avant l'adresse alloué
		fb=fb->next;
		mem_fb=fb->next;
	}
	void* mem_suiv=fb;
	mem_suiv+=fb->size;
	if (mem_suiv==mem){			//si ZL juste avant ZO 

		fb->size+=(size_t)*taille;		// aggrandit la ZL pour englober ZO
		mem_suiv+=*taille;
		if (mem_suiv==fb->next){	// vérifie si la zone suivant la ZO est libre si oui elle la fusionne
			fb->size+=fb->next->size;
			fb->next=fb->next->next;
			
		}
	}else{							// il n'y a pas de ZL précedente collé à la ZO
		struct fb *fbnew=mem; 		// créer un nouvelle ZL
		fbnew->size=(size_t)*taille;
		fbnew->next=mem_fb;
		fb->next=fbnew->next;
		if (fbnew+fbnew->size==fbnew->next){ // vérifie si la zone suivant la ZO est libre si oui elle la fusionne
			fbnew->size=fbnew->size+fbnew->next->size;
			fbnew->next=fbnew->next->next;
		}
	}
	}
}


struct fb* mem_fit_first(struct fb *list, size_t size) {
	struct fb* cherche= list;
	
	while ((cherche->next!=NULL)&&(cherche->size<size+sizeof(struct alloue))){
		cherche=cherche->next;
	}
	return cherche;
}

/* Fonction à faire dans un second temps
 * - utilisée par realloc() dans malloc_stub.c
 * - nécessaire pour remplacer l'allocateur de la libc
 * - donc nécessaire pour 'make test_ls'
 * Lire malloc_stub.c pour comprendre son utilisation
 * (ou en discuter avec l'enseignant)
 */
size_t mem_get_size(void *zone) {
	struct alloue *alloue= zone-sizeof(struct alloue);
	/* zone est une adresse qui a été retournée par mem_alloc() */
	/* la valeur retournée doit être la taille maximale que
	 * l'utilisateur peut utiliser dans cette zone */
	return alloue->size -sizeof(struct alloue);
}

/* Fonctions facultatives
 * autres stratégies d'allocation
 */
struct fb* mem_fit_best(struct fb *list, size_t size) {
	size_t min = list->size;			//Initialiser le minimum avec la première ZL
	struct fb * minList = list;
	while (list->next != NULL ){		// On parcours la liste de ZL pour trouver la plus petite
		list = list->next;
		if ((list->size >= size + sizeof(size_t)) && list->size <= min){
			min = list->size;			// On redéfinir le min
			minList = list;
		}
	}
	return minList;
}

struct fb* mem_fit_worst(struct fb *list, size_t size) {
	size_t max = list->size;			// Initialiser le maximum avec la première ZL
	struct fb * maxList = list;
	while (list->next != NULL ){		// On parcours la liste de ZL pour trouver la plus grande
		list = list->next;
		if ((list->size >= size + sizeof(size_t)) && list->size >= max){
			max = list->size;			// On redéfinir le max
			maxList = list;
		}
	}
	return maxList;
}